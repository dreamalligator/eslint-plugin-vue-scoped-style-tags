# eslint-plugin-vue-scoped-style-tags

ESLint rule plugin to require Vue scoped style tags.

## Installation

```bash
yarn add -D eslint-plugin-vue-scoped-style-tags
```

## Usage

`.eslintrc.js`:

```javascript
module.exports = {
  plugins: [
    'vue-scoped-style-tags',
  ],
  rules: {
    'vue-scoped-style-tags': 'error',
  },
};
```

Options:

* `always`
* `never`

```bash
$(yarn bin)/eslint --ext .vue
```

## Examples

Invalid `.vue` File:

```vue
<template>
🐥
</template>

<style>
body {
  background-color: red;
}
</style>
```

Valid `.vue` File:

```vue
<template>
🐥
</template>

<style scoped>
body {
  background-color: red;
}
</style>
```

## License

MIT
