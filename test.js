'use strict';

const RuleTester = require('eslint').RuleTester;
const rule = require('./index.js');

const ruleTester = new RuleTester({
  parser: require.resolve('vue-eslint-parser'),
  parserOptions: {
    ecmaVersion: 2015,
    sourceType: 'module',
  }
});

function main () {
  ruleTester.run('vue-scoped-style-tags', rule, {
    invalid: [
      {
        code: `<template>
🐥
</template>

<style>
body {
  background-color: red;
}
</style>`,
        errors: [
          { message: 'The <style> tag should have scope applied on line 5.' }
        ],
        filename: 'test.vue',
      },
      {
        code: `<template>
🐥
</template>

<style>
body {
  background-color: red;
}
</style>`,
        errors: [
          { message: 'The <style> tag should have scope applied on line 5.' }
        ],
        filename: 'test.vue',
        options: ['always']
      },
      {
        code: `<template>
🐥
</template>

<style scoped>
body {
  background-color: red;
}
</style>`,
        errors: [
          { message: 'The <style> tag should not have scope applied on line 5.' }
        ],
        filename: 'test.vue',
        options: ['never']
      }
    ],
    valid: [
      {
        code: `<template>
🐥
</template>

<style scoped>
body {
  background-color: red;
}
</style>`,
        filename: 'test.vue',
      },
      {
        code: `<template>
🐥
</template>

<style scoped>
body {
  background-color: red;
}
</style>`,
        filename: 'test.vue',
        options: ['always'],
      },
      {
        code: `<template>
🐥
</template>

<style>
body {
  background-color: red;
}
</style>`,
        filename: 'test.vue',
        options: ['never'],
      },
    ],
  });
};

main();
