'use strict';

module.exports = {
  meta: {
    docs: {
      description: 'ESLint rule plugin to require Vue scoped style tags.',
      url: 'https://gitlab.com/dreamalligator/eslint-plugin-vue-scoped-style-tags',
    },
    fixable: 'code',
    schema: [
      {
        enum: ['always', 'never'],
      },
    ],
    type: 'suggestion',
  },
  create: function (context) {
    const documentFragment = context.parserServices.getDocumentFragment &&
      context.parserServices.getDocumentFragment();

    if (!documentFragment) {
      return {};
    }

    const styleTagVElement = documentFragment.children
      .filter((node) => node.type === 'VElement')
      .find((element) => element.name === 'style');

    if (!styleTagVElement) {
      return {};
    }

    const hasScopedAttribute = styleTagVElement.startTag.attributes
      .find((attr) => attr.key.name === 'scoped') !== undefined;
    const requireScopedAttribute = context.options[0] !== 'never';

    if (hasScopedAttribute === requireScopedAttribute) {
      return {};
    }

    context.report({
      data: {
        line: styleTagVElement.loc.start.line,
      },
      message: requireScopedAttribute
        ? 'The <style> tag should have scope applied on line {{line}}.'
        : 'The <style> tag should not have scope applied on line {{line}}.',
      node: styleTagVElement,
    });

    return {};
  }
};
